# -*- coding: utf-8 -*-
from pysciebo import __version__


def test_version():
    assert __version__ == "1.1.0"
