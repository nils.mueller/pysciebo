# -*- coding: utf-8 -*-
"""
Integration test suite.
"""
import os
import tempfile
from pathlib import Path

import pytest

import pysciebo


@pytest.fixture
def local_file():
    return Path(__file__)


@pytest.fixture
def password():
    return os.environ["SCIEBO_PASSWORD"]


@pytest.fixture
def remote_file(local_file):
    return Path(local_file.name)


@pytest.fixture
def url():
    return os.environ["SCIEBO_URL"]


@pytest.fixture
def username():
    return os.environ["SCIEBO_USERNAME"]


def test_login(url, username, password):
    """
    On login, retrieving the logged in user returns an object that contains the
    correct username.
    """
    client = pysciebo.login(url, username, password)
    user = client.get_user(username)

    assert username in user["displayname"]


def test_upload(url, username, password, local_file, remote_file):
    """
    Test uploading a local file using the ScieboClient.
    """
    client = pysciebo.ScieboClient(url, username, password)
    result = client.upload(remote_file, local_file)

    assert result is True


def test_download(url, username, password, remote_file):
    """
    Test downloading a remote file using the ScieboClient.
    """
    client = pysciebo.ScieboClient(url, username, password)
    tmp_dir = tempfile.mkdtemp()
    local_file_path = Path(tmp_dir) / remote_file.name
    result = client.download(remote_file, local_file_path)

    assert result is True


def test_delete(url, username, password, remote_file):
    """
    Test deleting a remote file using the ScieboClient.
    """
    client = pysciebo.ScieboClient(url, username, password)
    result = client.delete(remote_file)

    assert result is True
